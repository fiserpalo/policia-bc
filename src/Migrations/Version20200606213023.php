<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200606213023 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE weekend_poll (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, priority INT NOT NULL, INDEX IDX_2C20ED4BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend_poll_weekend_schedule (weekend_poll_id INT NOT NULL, weekend_schedule_id INT NOT NULL, INDEX IDX_E4D8CCEACA300A54 (weekend_poll_id), INDEX IDX_E4D8CCEA76CE7050 (weekend_schedule_id), PRIMARY KEY(weekend_poll_id, weekend_schedule_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend_schedule (id INT AUTO_INCREMENT NOT NULL, end_poll DATE NOT NULL, friday DATE NOT NULL, saturday DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE weekend_poll ADD CONSTRAINT FK_2C20ED4BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE weekend_poll_weekend_schedule ADD CONSTRAINT FK_E4D8CCEACA300A54 FOREIGN KEY (weekend_poll_id) REFERENCES weekend_poll (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE weekend_poll_weekend_schedule ADD CONSTRAINT FK_E4D8CCEA76CE7050 FOREIGN KEY (weekend_schedule_id) REFERENCES weekend_schedule (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE weekend_poll_weekend_schedule DROP FOREIGN KEY FK_E4D8CCEACA300A54');
        $this->addSql('ALTER TABLE weekend_poll_weekend_schedule DROP FOREIGN KEY FK_E4D8CCEA76CE7050');
        $this->addSql('DROP TABLE weekend_poll');
        $this->addSql('DROP TABLE weekend_poll_weekend_schedule');
        $this->addSql('DROP TABLE weekend_schedule');
    }
}
