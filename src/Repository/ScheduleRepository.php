<?php

namespace App\Repository;

use App\Entity\Schedule;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Schedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Schedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Schedule[]    findAll()
 * @method Schedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Schedule::class);
    }


    public function findScheduleDays(\DateTime $startDate, \DateTime $endDate)
    {

        $result = $this->createQueryBuilder('s')
            ->Where('s.dutyDate BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->addOrderBy('s.dutyDate','ASC')
            ->getQuery()
            ->getResult();

        $resultArr = [];
        foreach ($result as $row)
        {
            $day = $row->getDutyDay();
            if (array_key_exists($day, $resultArr)){
                $resultArr[$day][count($resultArr[$day])] = $row;
            }else{
                $resultArr[$day] = [];
                $resultArr[$day][count($resultArr[$day])] = $row;
            }
        }

       return $resultArr;
    }
    public function findMyScheduleDays(\DateTime $startDate, \DateTime $endDate,User $user)
    {

        return $this->createQueryBuilder('s')
            ->Where('s.user = :user')
            ->andWhere('s.dutyDate BETWEEN :startDate AND :endDate')
            ->setParameter('user', $user)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->getQuery()
            ->getResult();


    }


    /*
    public function findOneBySomeField($value): ?Schedule
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
