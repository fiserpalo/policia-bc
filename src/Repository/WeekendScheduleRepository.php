<?php

namespace App\Repository;

use App\Entity\WeekendSchedule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeekendSchedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeekendSchedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeekendSchedule[]    findAll()
 * @method WeekendSchedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeekendScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeekendSchedule::class);
    }

    // /**
    //  * @return WeekendSchedule[] Returns an array of WeekendSchedule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WeekendSchedule
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
