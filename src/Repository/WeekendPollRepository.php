<?php

namespace App\Repository;

use App\Entity\WeekendPoll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeekendPoll|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeekendPoll|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeekendPoll[]    findAll()
 * @method WeekendPoll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeekendPollRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeekendPoll::class);
    }

    // /**
    //  * @return WeekendPoll[] Returns an array of WeekendPoll objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WeekendPoll
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
