<?php


namespace App\Constants;


class DutyDays
{
    public const SUNDAY = 'sunday';
    public const MONDAY = 'monday';
    public const TUESDAY = 'tuesday';
    public const WEDNESDAY = 'wednesday';
    public const THURSDAY = 'thursday';

    public static function getConstants()
    {
        return [
            'SUNDAY' => 'sunday',
            'MONDAY' => 'monday',
            'TUESDAY' => 'tuesday',
            'WEDNESDAY' => 'wednesday',
            'THURSDAY' => 'thursday'
        ];
    }


}