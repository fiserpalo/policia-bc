<?php


namespace App\Constants;


class Blocks
{
    public const A = 'A';
    public const B = 'B';
    public const C = 'C';
    public const D = 'D';

    public static function getConstants()
    {
        return [
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D'
        ];
    }
}