<?php

namespace App\Entity;

use App\Repository\WeekendPollRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeekendPollRepository::class)
 */
class WeekendPoll
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class , fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @ORM\ManyToMany(targetEntity=WeekendSchedule::class, fetch="EAGER")
     */
    private $weekSchedule;

    /**
     * @ORM\Column(type="date")
     */
    private $pollDate;

    public function __construct()
    {
        $this->weekSchedule = new ArrayCollection();
        $this->setPollDate(new \DateTime('now'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection|WeekendSchedule[]
     */
    public function getWeekSchedule(): Collection
    {
        return $this->weekSchedule;
    }

    public function addWeekSchedule(WeekendSchedule $weekSchedule): self
    {
        if (!$this->weekSchedule->contains($weekSchedule)) {
            $this->weekSchedule[] = $weekSchedule;
        }

        return $this;
    }

    public function removeWeekSchedule(WeekendSchedule $weekSchedule): self
    {
        if ($this->weekSchedule->contains($weekSchedule)) {
            $this->weekSchedule->removeElement($weekSchedule);
        }

        return $this;
    }

    public function getPollDate(): ?\DateTimeInterface
    {
        return $this->pollDate;
    }

    public function setPollDate(\DateTimeInterface $pollDate): self
    {
        $this->pollDate = $pollDate;

        return $this;
    }
}
