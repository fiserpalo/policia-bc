<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Duty
 *
 * @ORM\Table(name="duty")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DutyRepository")
 */
class Duty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $dutyDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $dutyDuration;

    /**
     * @ORM\Column(type="time")
     */
    private $dutyStart;

    /**
     * @return mixed
     */
    public function getDutyDate()
    {
        return $this->dutyDate;
    }

    /**
     * @param mixed $dutyDate
     * @return Duty
     */
    public function setDutyDate($dutyDate)
    {
        $this->dutyDate = $dutyDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDutyDuration()
    {
        return $this->dutyDuration;
    }

    /**
     * @param mixed $dutyDuration
     * @return Duty
     */
    public function setDutyDuration($dutyDuration)
    {
        $this->dutyDuration = $dutyDuration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDutyStart()
    {
        return $this->dutyStart;
    }

    /**
     * @param mixed $dutyStart
     * @return Duty
     */
    public function setDutyStart($dutyStart)
    {
        $this->dutyStart = $dutyStart;
        return $this;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

}
