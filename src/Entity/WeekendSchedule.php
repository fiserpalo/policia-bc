<?php

namespace App\Entity;

use App\Repository\WeekendScheduleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeekendScheduleRepository::class)
 */
class WeekendSchedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $endPoll;

    /**
     * @ORM\Column(type="date")
     */
    private $friday;

    /**
     * @ORM\Column(type="date")
     */
    private $saturday;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEndPoll(): ?\DateTimeInterface
    {
        return $this->endPoll;
    }

    public function setEndPoll(\DateTimeInterface $endPoll): self
    {
        $this->endPoll = $endPoll;

        return $this;
    }

    public function getFriday(): ?\DateTimeInterface
    {
        return $this->friday;
    }

    public function setFriday(\DateTimeInterface $friday): self
    {
        $this->friday = $friday;

        return $this;
    }

    public function getSaturday(): ?\DateTimeInterface
    {
        return $this->saturday;
    }

    public function setSaturday(\DateTimeInterface $saturday): self
    {
        $this->saturday = $saturday;

        return $this;
    }
}
