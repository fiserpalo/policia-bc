<?php

namespace App\Entity;

use App\Repository\ScheduleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScheduleRepository::class)
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="schedules" , cascade={"all"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date")
     */
    private $dutyDate;

    /**
     * @ORM\Column(type="time")
     */
    private $dutyStartTime;

    /**
     * @ORM\Column(type="float")
     */
    private $dutyDuration;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $dutyDay;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $dutyCorridor;

    /**
     * @ORM\OneToOne(targetEntity=Schedule::class, inversedBy="scheduleSwitchTo", cascade={"persist", "remove"}, cascade={"all"}, fetch="EAGER")
     */
    private $switchSchedule;

    /**
     * @ORM\OneToOne(targetEntity=Schedule::class, mappedBy="switchSchedule", cascade={"persist", "remove"}, cascade={"all"}, fetch="EAGER")
     */
    private $scheduleSwitchTo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDutyDate(): ?\DateTimeInterface
    {
        return $this->dutyDate;
    }

    public function setDutyDate(\DateTimeInterface $dutyDate): self
    {
        $this->dutyDate = $dutyDate;

        return $this;
    }

    public function getDutyStartTime(): ?\DateTimeInterface
    {
        return $this->dutyStartTime;
    }

    public function setDutyStartTime(\DateTimeInterface $dutyStartTime): self
    {
        $this->dutyStartTime = $dutyStartTime;

        return $this;
    }

    public function getDutyDuration(): ?float
    {
        return $this->dutyDuration;
    }

    public function setDutyDuration(float $dutyDuration): self
    {
        $this->dutyDuration = $dutyDuration;

        return $this;
    }

    public function getDutyDay(): ?string
    {
        return $this->dutyDay;
    }

    public function setDutyDay(string $dutyDay): self
    {
        $this->dutyDay = $dutyDay;

        return $this;
    }

    public function getDutyCorridor(): ?bool
    {
        return $this->dutyCorridor;
    }

    public function setDutyCorridor(?bool $dutyCorridor): self
    {
        $this->dutyCorridor = $dutyCorridor;

        return $this;
    }

    public function getSwitchSchedule(): ?self
    {
        return $this->switchSchedule;
    }

    public function setSwitchSchedule(?self $switchSchedule): self
    {
        $this->switchSchedule = $switchSchedule;

        return $this;
    }

    public function getScheduleSwitchTo(): ?self
    {
        return $this->scheduleSwitchTo;
    }

    public function setScheduleSwitchTo(?self $scheduleSwitchTo): self
    {
        $this->scheduleSwitchTo = $scheduleSwitchTo;
        return $this;
    }
}
