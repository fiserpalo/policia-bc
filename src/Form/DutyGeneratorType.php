<?php

namespace App\Form;

use App\Entity\Duty;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DutyGeneratorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction('new')
            ->setMethod('POST')
            ->add('template',EntityType::class,[
                'attr' => ['class' => 'form-control'],
                'class' => Duty::class,
                'choice_label' => 'Name',
               // 'choices' => $group->getUsers(),
            ])
            ->add('members', EntityType::class, [
                'attr' => ['class' => 'form-control'],
                'class' => User::class,
                'choice_label' => 'Name',
                'multiple' => true
            ])
            ->add('startdate', DateType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true,
                'widget' => 'single_text',
            ])
            ->add('enddate', DateType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true,
                'widget' => 'single_text',
            ]);


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
