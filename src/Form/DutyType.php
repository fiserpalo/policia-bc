<?php

namespace App\Form;

use App\Constants\DutyDays;
use App\Entity\Duty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DutyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null,[
                'attr' => ['class' => 'form-control']
            ])
            ->add('dutyDate',  ChoiceType::class,[
                'attr' => ['class' => 'form-control'],
                'choices'  => DutyDays::getConstants()
            ])
            ->add('dutyDuration', null,[
                'attr' => ['class' => 'form-control']
            ])
            ->add('dutyStart', null,[
                'attr' => ['class' => 'form-control']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Duty::class,
        ]);
    }
}
