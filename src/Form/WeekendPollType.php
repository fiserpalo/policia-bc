<?php

namespace App\Form;

use App\Entity\WeekendPoll;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekendPollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction('new')
            ->setMethod('POST')
            ->add('start', DateType::class, [
                'attr' => ['class' => 'form-control mx-sm-3 mb-2'],
                'required' => true,
                'widget' => 'single_text',
                'label' => 'Od',
            ])
            ->add('end', DateType::class, [
                'attr' => ['class' => 'form-control mx-sm-3 mb-2'],
                'required' => true,
                'widget' => 'single_text',
                'label' => 'Do',
            ])
            ->add('finish', DateType::class, [
                'attr' => ['class' => 'form-control mx-sm-3 mb-2'],
                'required' => true,
                'widget' => 'single_text',
                'label' => 'Koniec hlasovania',
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
