<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Schedule;
use App\Entity\User;
use App\Form\PostType;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function index()
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $this->getUser()->getId()]);
        $data = $user->getSchedules()->toArray();
        if (in_array("ROLE_VIEWER",$this->getUser()->getRoles()))
        {
            return $this->redirectToRoute('punishment_index');
        }

        return $this->render('admin/index.html.twig', [
            'schedules' => $data,
        ]);
    }


    /**
     * @Route("/admin/switch/{schedule}/{acceptance}", name="admin_switch")
     */
    public function switch(Request $request, Schedule $schedule): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $this->getUser()->getId()]);
        $switchSchedule = $schedule->getSwitchSchedule();
        $switchUser = $switchSchedule->getUser();

        $acceptance = $request->get('acceptance');
        if ($acceptance == 'true') {
            $switchSchedule->setuser($user);
            $schedule->setUser($switchUser);
        }
            $switchSchedule->setScheduleSwitchTo(null);
            $schedule->setSwitchSchedule(null);

            $em->flush();


        return $this->redirectToRoute('admin_index');
    }


}
