<?php

namespace App\Controller;

use App\Entity\Punishment;
use App\Entity\User;
use App\Form\PunishmentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PunishmentController extends AbstractController
{
    /**
     * @Route("/admin/punishment", name="punishment_index")
     */
    public function index()
    {
        return $this->render('punishment/index.html.twig', [
            'controller_name' => 'PunishmentController',
            'punishments' => $this->getDoctrine()->getManager()->getRepository(Punishment::class)->findAll()
        ]);
    }

    /**
     * @Route("/admin/punishment/new", name="punishment_new")
     */
    public function new(Request $request): Response
    {
        $punishment = new Punishment();
        $punishment->setDay(new \DateTime());
        $form = $this->createForm(PunishmentType::class, $punishment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $punishment->setDay(new \DateTime());
            $punishment->setAutor($this->getUser());

            $entityManager->persist($punishment);
            $entityManager->flush();

            return $this->redirectToRoute('punishment_index');
        }

        return $this->render('punishment/new.html.twig', [
            'punishment' => $punishment,
            'form' => $form->createView(),
        ]);
    }
}
