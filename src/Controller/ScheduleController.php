<?php

namespace App\Controller;

use App\Entity\Duty;
use App\Entity\Schedule;
use App\Entity\WeekendPoll;
use App\Entity\WeekendSchedule;
use App\Form\DutyGeneratorType;
use App\Form\DutyType;
use App\Form\ScheduleType;
use App\Form\WeekendPollType;
use DateInterval;
use DatePeriod;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class ScheduleController extends AbstractController
{
    /**
     * @Route("/admin/schedule/", name="schedule_index")
     * @throws \Exception
     */
    public function index(Request $request): Response
    {
        $scheduleRepo = $this->getDoctrine()->getRepository(Schedule::class);


        if ($request->get("date")) {
            if ($request->get("type") == 'next') {
                $startDate = New DateTime($request->get("date"));
                $startDate->modify('+1 day');
                $endDate = New DateTime($request->get("date"));
                $endDate->modify('+1 week');
            } else {
                $startDate = New DateTime($request->get("date"));
                $startDate->modify('-1 week');
                $endDate = New DateTime($request->get("date"));
                $endDate->modify('-1 day');
            }


        } else {
            $startDate = new DateTime('Monday this week');
            $startDate->modify('-1 day');
            $endDate = new DateTime('Sunday this week');
            $endDate->modify('-1 day');
        }

        $mySchedule = $scheduleRepo->findMyScheduleDays($startDate, $endDate, $this->getUser());
        if(empty($mySchedule))
        {
            $mySchedule = null;
        }
        return $this->render('schedule/index.html.twig', [
            'linkPrev' => $startDate->format('Y-m-d'),
            'linkNext' => $endDate->format('Y-m-d'),
            'schedules' => $scheduleRepo->findScheduleDays($startDate, $endDate),
            'mySchedule' => $mySchedule ? $mySchedule[0] : '',
        ]);
    }

    /**
     * @Route("/admin/schedule/download", name="schedule_download")
     */
    public function download(Request $request): Response
    {
        $samplePdf = new File('file/example.csv');

        return $this->file($samplePdf);
    }

    /**
     * @Route("/admin/schedule/new", name="schedule_new")
     */
    public function new(Request $request): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $schedule = new Schedule();
        $form = $this->createForm(DutyGeneratorType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $scheduleData = $form->getData();
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($scheduleData['startdate'], $interval, $scheduleData['enddate']);
            foreach ($period as $dt) {
                if (strtolower($dt->format("l")) == $scheduleData['template']->getDutyDate()) {
                    foreach ($scheduleData['members'] as $user) {
                        $newSchedule = new Schedule();
                        $newSchedule->setDutyCorridor(false);
                        $newSchedule->setUser($user);
                        $newSchedule->setDutyDate($dt);
                        $newSchedule->setDutyDuration($scheduleData['template']->getDutyDuration());
                        $newSchedule->setDutyDay($scheduleData['template']->getDutyDate());
                        $newSchedule->setDutyStartTime($scheduleData['template']->getDutyStart());
                        $entityManager->persist($newSchedule);
                    }
                }
            }
            $entityManager->flush();
            return $this->redirectToRoute('schedule_index');
        }

        return $this->render('schedule/new.html.twig', [
            'schedule' => $schedule,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/schedule/weekend", name="schedule_weekend")
     */
    public function weekend(Request $request): Response
    {
        $scheduleRepo = $this->getDoctrine()->getRepository(WeekendSchedule::class);
        $pollRepo = $this->getDoctrine()->getRepository(WeekendPoll::class);
        return $this->render('schedule/weekend/index.html.twig', [
            'schedules' => $scheduleRepo->findAll(),
        ]);
    }

    /**
     * @Route("/admin/schedule/weekend/new", name="schedule_weekend_poll")
     */
    public function newWeekend(Request $request): Response
    {
        $pollRepo = $this->getDoctrine()->getRepository(WeekendPoll::class);
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(WeekendPollType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pollData = $form->getData();
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($pollData['start'], $interval, $pollData['end']);
            foreach ($period as $dt) {
                if (strtolower($dt->format("l")) == 'friday') {
                    $friday = new DateTime($dt->format('Y-m-d'));
                    $saturday = new DateTime($dt->format('Y-m-d'));
                    $saturday->modify('+1 day');
                    $weekednSchedule = new WeekendSchedule();
                    $weekednSchedule->setEndPoll($pollData['finish']);
                    $weekednSchedule->setFriday($friday);
                    $weekednSchedule->setSaturday($saturday);
                    $entityManager->persist($weekednSchedule);

                }
            }

            $entityManager->flush();
            return $this->redirectToRoute('schedule_index');
        }


        return $this->render('schedule/weekend/new.html.twig', [
            'form' => $form->createView(),
            'polls' => $pollRepo->findAll(),
        ]);
    }


    /**
     * @Route("/admin/schedule/weekend/generate", name="schedule_weekend_generate")
     */
    public function weekendGenerate(Request $request): Response
    {
        $scheduleRepo = $this->getDoctrine()->getRepository(WeekendSchedule::class);
        $pollRepo = $this->getDoctrine()->getRepository(WeekendPoll::class);
        $weekend = $scheduleRepo->findAll();
        $polls = $pollRepo->findAll();

        $scheduleArray = array();
        for ($i = 3; $i >= 0; $i--) {
            foreach ($weekend as $day) {
                if(array_key_exists($day->getFriday()->format('m.d.Y'),$scheduleArray))
                {
                    if (count($scheduleArray[$day->getFriday()->format('m.d.Y')])==2)
                    {
                        break;
                    }
                }
                foreach ($polls as $poll) {
                    if ($poll->getWeekSchedule()[0] == $day && $poll->getPriority() == $i) {
                        $scheduleArray[$day->getFriday()->format('m.d.Y')] = [$poll->getUser()];
                        $scheduleArray[$day->getSaturday()->format('m.d.Y')] = [$poll->getUser()];
                    }
                }

            }
        }

        return $this->redirectToRoute('schedule_weekend_poll');
    }


    /**
     * @Route("/admin/schedule/weekend/{weekendSchedule}/", name="request_weekend")
     * @param Request $request
     * @param WeekendSchedule $weekendSchedule
     * @return Response
     */
    public function requestDuty(Request $request, WeekendSchedule $weekendSchedule): Response
    {
        $wpRepo = $this->getDoctrine()->getRepository(WeekendPoll::class);
        $em = $this->getDoctrine()->getManager();

        $weekendPoll = $wpRepo->findBy(['user' => $this->getUser()]);

        if (count($weekendPoll) <= 3) {
            $newWeekendPoll = new WeekendPoll();
            $newWeekendPoll->setUser($this->getUser());
            $newWeekendPoll->setPriority(3 - count($weekendPoll));
            $newWeekendPoll->addWeekSchedule($weekendSchedule);

            $em->persist($newWeekendPoll);
            $em->flush();
        }
        return $this->redirectToRoute('schedule_weekend');

    }


    private function getDay(DateTime $startDate, string $day)
    {
        do {
            $startDate->modify('+1 day');
        } while (strtolower($startDate->format('l')) != $day);
        return $startDate;
    }


}
