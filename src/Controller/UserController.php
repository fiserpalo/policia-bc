<?php

namespace App\Controller;

use App\Entity\Schedule;
use App\Entity\User;
use App\Form\UploadUsersType;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/user", name="user_index")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'users' => $this->getDoctrine()->getRepository(User::class)->findAll(),
        ]);
    }

    /**
     * @Route("/admin/user/new", name="user_new")
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPhone(substr($user->getPhone(), 0, 14));
            $user->setPhoto('/img/user/avatar.png');
            $user->setPassword('jahoda123');
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/user_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/remove/{user}", name="user_remove")
     */
    public function remove(Request $request, User $user): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user_index');
    }


    /**
     * @Route("/admin/user/load", name="user_load")
     */
    public function loadCSV(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UploadUsersType::class);
        $form->handleRequest($request);
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $serializer = $this->get('serializer');
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            $data = $serializer->decode(file_get_contents($file->getPathname()), 'csv');
            foreach ($data as $userData) {
                $status = true;
                foreach ($users as $user) {
                    if ($userData['email'] == $user->getEmail()) {
                        $status = false;
                    }
                }
                if($status) {
                    $user = new User();
                    $user->setDoormRoom($userData['doormRoom']);
                    $user->setEmail($userData['email']);
                    $user->setName($userData['name']);
                    $user->setPassword('jahoda123')->hashPassword();
                    $user->setPhoto('/img/user/avatar.png');
                    $user->setPhone('0' . str_replace(' ', '', substr($userData['phone'], 0, 14)));
                    $em->persist($user);
                }
            }
               $em->flush();
            return $this->redirectToRoute('user_index');
        }
        return $this->render('user/load.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/user/toogle/{id}", name="user_toogle_corridor")
     */
    public function toogleCorridor(Request $request, Schedule $schedule): Response
    {
        $schedule->setDutyCorridor(!$schedule->getDutyCorridor());

        $em = $this->getDoctrine()->getManager();
        $em->persist($schedule);
        $em->flush();

        return $this->redirectToRoute('schedule_index');
    }


    /**
     * @Route("/admin/user/switch/{schedule}/{to}", name="user_request_switch")
     */
    public function requestSwitch(Request $request, Schedule $schedule, Schedule $to): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $this->getUser()->getId()]);

        $schedule->setSwitchSchedule($to);
        $to->setScheduleSwitchTo($schedule);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('schedule_index');
    }


    /**
     * @Route("/user/profile/", name="user_profile")
     */
    public function profile(Request $request): Response
    {
        return $this->render('user/profile/index.html.twig', [
        ]);
    }

    /**
     * @Route("/user/password/update", name="user_password_update")
     */
    public function passwords(Request $request): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('id'));
        $pass1 = $request->get('password1');
        $pass2 = $request->get('password2');
        if ($pass1 = $pass2) {
            $user->setPassword($pass1)->hashPassword();
        }

        return $this->redirectToRoute('user_profile');
    }

    /**
     * @Route("/user/profile/edit", name="user_profile_update")
     */
    public function editProfile(Request $request): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('id'));
        $user->setName($request->get('name'));
        $user->setEmail($request->get('email'));
        $user->setPhone($request->get('phone'));
        $user->setDoormRoom($request->get('dormRoom'));
//        $user->addRole('ROLE_ADMIN');
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();


        return $this->render('user/profile/index.html.twig', [
        ]);
    }
}
