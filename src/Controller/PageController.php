<?php

namespace App\Controller;


use App\Entity\Post;
use App\Entity\User;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PageController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function index(Request $request) :Response
    {
        $postRepo= $this->getDoctrine()->getRepository(Post::class);
        $userRepo= $this->getDoctrine()->getRepository(User::class);
        return $this->render('page/index.html.twig', [
            'posts' => $postRepo->findBy( ['active' => 1]),
            'users' => $userRepo->findAll()
        ]);
    }

    /**
     * @Route("/post/{id}", name="post")
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function post(Request $request, Post $post) :Response
    {
        $postRepo= $this->getDoctrine()->getRepository(Post::class);
        return $this->render('page/post.html.twig', [
            'post' => $post,
        ]);
    }

}
